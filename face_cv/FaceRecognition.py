import face_recognition
import os
import cv2
import numpy as np
import math

def face_confidence(face_distance, face_match_threshold=0.6):
    range = (1.0 - face_match_threshold)
    linear_val = (1.0 - face_distance) / (range * 2.0)

    if face_distance > face_match_threshold:
        return str(round(linear_val * 100, 2)) + '%'
    else:
        value = (linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))) * 100
        return str(round(value, 2)) + '%'

class FaceRecognition:
    face_locations = []
    face_encoding = []
    face_names = []
    known_face_encodings = []
    known_face_names = []
    process_current_frame = True
    path_face = 'face_cv/faces'

    def __init__(self):
        self.encode_faces()

    def encode_faces(self):
        # load all face source that will be match
        for image in os.listdir(self.path_face):
            if (image.endswith(".png") or image.endswith(".jpg") or image.endswith(".jpeg")) :
                face_image = face_recognition.load_image_file(f'{self.path_face}/{image}')
                face_encoding = face_recognition.face_encodings(face_image)

                if(len(face_encoding)> 0):
                    self.known_face_encodings.append(face_encoding[0])
                    self.known_face_names.append(image)
                else:
                    print("cannot detect face at image : "+ image)

        print(self.known_face_names)

    def run_recognition(self):
        # capture frames from a camera
        cap = cv2.VideoCapture(0)

        # loop runs if capturing has been initialized.
        while 1:

            # reads frames from a camera
            ret, img = cap.read()

            self.matchFaceAndEyes(img)

            self.matchFace(img)

            # Display an image in a window
            cv2.imshow('img', img)

            # Wait for Esc key to stop
            k = cv2.waitKey(30) & 0xff
            if k == 27 or k == ord('q'):
                break

        # Close the window
        cap.release()

        # De-allocate any associated memory usage
        cv2.destroyAllWindows()

    def matchFaceAndEyes(self, frame):
        # https://github.com/Itseez/opencv/blob/master
        # /data/haarcascades/haarcascade_frontalface_default.xml
        face_cascade = cv2.CascadeClassifier('face_cv/haarcascade/haarcascade_frontalface_default.xml')

        # /data/haarcascades/haarcascade_eye.xml
        # Trained XML file for detecting eyes
        eye_cascade = cv2.CascadeClassifier('face_cv/haarcascade/haarcascade_eye.xml')

        # convert to gray scale of each frames
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        # Detects faces of different sizes in the input image
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        for (x, y, w, h) in faces:
            # To draw a rectangle in a face
            # cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 255, 0), 2)
            # cv2.putText(frame, "unknown", (x + 6, y - 6), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1)

            # Detects eyes of different sizes in the input image
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = frame[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)

            # To draw a rectangle in eyes
            for (ex, ey, ew, eh) in eyes:
                cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 127, 255), 2)

    def matchFace(self, frame):

        small_frame = cv2.resize(frame, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_AREA)
        rgb_small_frame = small_frame[:, :, ::-1]

        self.face_locations = face_recognition.face_locations(rgb_small_frame)
        self.face_encoding = face_recognition.face_encodings(rgb_small_frame, self.face_locations)

        self.face_names = []
        for face_encoding in self.face_encoding:
            matches = face_recognition.compare_faces(self.known_face_encodings, face_encoding)
            name = 'Tidak Dikenal'
            confidence = '0%'

            face_distances = face_recognition.face_distance(self.known_face_encodings, face_encoding)
            best_match_index = np.argmin(face_distances)

            if matches[best_match_index]:
                confidence = face_confidence(face_distances[best_match_index])
                file_name = self.known_face_names[best_match_index].split(".")
                name = file_name[0]

            self.face_names.append(f'{name} ({confidence})')

            for (top, right, bottom, left), name in zip(self.face_locations, self.face_names):
                top *= 4
                right *= 4
                bottom *= 4
                left *= 4

                cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), -1)
                cv2.putText(frame, name, (left + 6, bottom - 6), cv2.FONT_HERSHEY_DUPLEX, 0.8, (255, 255, 255), 1)